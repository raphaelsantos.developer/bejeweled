using System.Collections;
using com.raphael.bejeweled.Systems;
using NUnit.Framework;
using UnityEngine;
using UnityEngine.TestTools;

namespace com.raphael.bejeweled.Tests.Systems
{
    public class UnityEventSystemTest
    {
        private UnityEventSystem _unityEventSystem;
        
        [SetUp]
        public void SetUp()
        {
            _unityEventSystem = new UnityEventSystem();
        }
        [UnityTest]
        public IEnumerator OnUpdate_AddFunction_FunctionIsCalled()
        {
            bool functionWasCalled = false;
            _unityEventSystem.OnUpdate += () => functionWasCalled = true;
            
            yield return new WaitForSeconds(1);
            
            Assert.IsTrue(functionWasCalled);
        }
        

        [UnityTest]
        public IEnumerator CallLater_AddFunction_FunctionIsCalled()
        {
            bool functionWasCalled = false;
            _unityEventSystem.CallLater(.5f, () => functionWasCalled = true);
            
            yield return new WaitForSeconds(1);
            
            Assert.IsTrue(functionWasCalled);
        }
        
        [UnityTest]
        public IEnumerator StartCoroutine_AddFunction_FunctionIsCalled()
        {
            bool functionWasCalled = false;
            _unityEventSystem.StartCoroutine(TestCoroutine());
            
            yield return new WaitForSeconds(.5f);
            
            Assert.IsTrue(functionWasCalled);

            IEnumerator TestCoroutine()
            {
                yield return new WaitForSeconds(.2f);
                functionWasCalled = true;
            }
        }
    }
}