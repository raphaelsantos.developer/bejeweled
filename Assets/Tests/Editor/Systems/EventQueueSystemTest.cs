using System;
using com.raphael.bejeweled.Systems;
using NUnit.Framework;

namespace com.raphael.bejeweled.Tests.Systems
{
    public class EventQueueSystemTest
    {
        private ActionQueueSystem _actionQueueSystem;
        
        [SetUp]
        public void SetUp()
        {
            _actionQueueSystem = new ActionQueueSystem();
        }
        
        [Test]
        public void Enqueue_NewEvent_EventIsStarted()
        {
            bool wasStarted = false;
            _actionQueueSystem.Enqueue(action => wasStarted = true);
            
            Assert.IsTrue(wasStarted);
        }
        
        [Test]
        public void Enqueue_TwoEvents_NextEventIsStarted()
        {
            bool wasStarted = false;
            Action nextEventAction = null;
            _actionQueueSystem.Enqueue(action => nextEventAction = action);
            _actionQueueSystem.Enqueue(action => wasStarted = true);
            
            Assert.IsFalse(wasStarted);
            
            nextEventAction.Invoke();
            
            Assert.IsTrue(wasStarted);
        }
        
        [Test]
        public void Enqueue_NewEventNoWaitPreviously_EventIsStarted()
        {
            bool wasStarted = false;
            Action nextEventAction = null;
            _actionQueueSystem.Enqueue(action => nextEventAction = action);
            _actionQueueSystem.Enqueue(action => action.Invoke());
            _actionQueueSystem.Enqueue(action => wasStarted = true);
            
            Assert.IsFalse(wasStarted);
            
            nextEventAction.Invoke();
            
            Assert.IsTrue(wasStarted);
        }
    }
}