using com.raphael.bejeweled.Systems;
using NUnit.Framework;
using UnityEngine;

namespace com.raphael.bejeweled.Tests.Systems
{
    public class PoolingSystemTest
    {
        private PoolingSystem _poolingSystem;
        private GameObject _testObject;
        
        [SetUp]
        public void SetUp()
        {
            _poolingSystem = new PoolingSystem();
            _testObject = new GameObject();
        }

        [Test]
        public void Instantiate_NewObject_NewObjectIsCreate()
        {
            var gameObject = new GameObject();
            var newObject = _poolingSystem.Instantiate(gameObject);
            
            Assert.AreNotEqual(newObject, gameObject);
        }
        
        [Test]
        public void Destroy_DestroyObject_NewObjectIsCreate()
        {
            _poolingSystem.Destroy(_testObject);
            
            Assert.IsFalse(_testObject.activeSelf);
        }
        
        [Test]
        public void Instantiate_OldObject_OldObjectIsReturned()
        {
            var newObject = _poolingSystem.Instantiate(_testObject);
            _poolingSystem.Destroy(newObject);
            var oldObject = _poolingSystem.Instantiate(_testObject);
            
            Assert.AreEqual(newObject, oldObject);
        }
        
        [Test]
        public void Instantiate_PositionValues_ObjectIsInstantiateInTheRightPlace()
        {

            var newObject =
                _poolingSystem.Instantiate(_testObject, Vector3.one, Quaternion.identity, _testObject.transform);
            
            Assert.AreEqual(Vector3.one, newObject.transform.position);
            Assert.AreEqual(Quaternion.identity, newObject.transform.rotation);
            Assert.AreEqual(_testObject.transform, newObject.transform.parent);
        }
    }
}