using System.Collections.Generic;
using com.raphael.bejeweled.Gameplay;
using com.raphael.bejeweled.Utils;
using NUnit.Framework;
using UnityEditor;

namespace Tests.Editor.Utils
{
    public class BoardUtilsTest
    {
        private Board _board;

        [SetUp]
        public void SetUp()
        {
            _board = AssetDatabase.LoadAssetAtPath<Board>("Assets/ScriptableObjects/Board.asset");
        }
        
        /// <summary>
        /// Check for horizontal matches
        /// |99900000|
        /// </summary>
        [Test]
        public void CheckHorizontalMatches_HasOneHorizontalMatch_OneMatchIsReturned()
        {
            _board.Initialize();
            _board.ReplaceVerticalPiece(99, 0, -1, 0);
            _board.ReplaceVerticalPiece(99, 1, -1, 0);
            _board.ReplaceVerticalPiece(99, 2, -1, 0);

            var matches = new List<Match>();
            _board.CheckHorizontalMatchesNonAlloc(0, 0, matches);
            
            Assert.AreEqual(1, matches.Count);
            Assert.AreEqual(0, matches[0].From);
            Assert.AreEqual(2, matches[0].To);
        }
        
        /// <summary>
        /// Check for horizontal matches
        /// |99988800|
        /// </summary>
        [Test]
        public void CheckHorizontalMatches_HasTwoHorizontalMatch_TwoMatchIsReturned()
        {
            _board.Initialize();
            _board.ReplaceVerticalPiece(99, 0, -1, 0);
            _board.ReplaceVerticalPiece(99, 1, -1, 0);
            _board.ReplaceVerticalPiece(99, 2, -1, 0);
            
            _board.ReplaceVerticalPiece(98, 3, -1, 0);
            _board.ReplaceVerticalPiece(98, 4, -1, 0);
            _board.ReplaceVerticalPiece(98, 5, -1, 0);

            var matches = new List<Match>();
            _board.CheckHorizontalMatchesNonAlloc(0, 2 , matches);
            
            Assert.AreEqual(2, matches.Count);
            Assert.AreEqual(0, matches[0].From);
            Assert.AreEqual(2, matches[0].To);
            Assert.AreEqual(3, matches[1].From);
            Assert.AreEqual(5, matches[1].To);
        }
        
        /// <summary>
        /// Check for horizontal and vertical matches
        /// |900|
        /// |900|
        /// |900|
        /// </summary>
        [Test]
        public void CheckVerticalMatches_HasOneVerticalMatch_OneMatchIsReturned()
        {
            _board.Initialize();
            _board.ReplaceVerticalPiece(99, 0, -1, 0);
            _board.ReplaceVerticalPiece(99, 0, -1, 1);
            _board.ReplaceVerticalPiece(99, 0, -1, 2);

            var matches = new List<Match>();
            _board.CheckVerticalMatchesNonAlloc(0, 0, matches);
            
            Assert.AreEqual(1, matches.Count);
            Assert.AreEqual(0, matches[0].From);
            Assert.AreEqual(2, matches[0].To);
        }
        
        /// <summary>
        /// Check for horizontal and vertical matches
        /// |900|
        /// |900|
        /// |900|
        /// |800|
        /// |800|
        /// |800|
        /// </summary>
        [Test]
        public void CheckVerticalMatches_HasTwoVerticalMatch_TwoMatchIsReturned()
        {
            _board.Initialize();
            _board.ReplaceVerticalPiece(99, 0, -1, 0);
            _board.ReplaceVerticalPiece(99, 0, -1, 1);
            _board.ReplaceVerticalPiece(99, 0, -1, 2);
            
            _board.ReplaceVerticalPiece(98, 0, -1, 3);
            _board.ReplaceVerticalPiece(98, 0, -1, 4);
            _board.ReplaceVerticalPiece(98, 0, -1, 5);

            var matches = new List<Match>();
            _board.CheckVerticalMatchesNonAlloc(0, 2, matches);
            
            Assert.AreEqual(2, matches.Count);
            Assert.AreEqual(0, matches[0].From);
            Assert.AreEqual(2, matches[0].To);
            Assert.AreEqual(3, matches[1].From);
            Assert.AreEqual(5, matches[1].To);
        }
        
        [Test]
        public void GetPiecesFromMatches_TwoMatches_SevenPiecesAreReturned()
        {
            var matches = new List<Match>(2)
            {
                new Match()
                {
                    Axis = Match.BoardAxis.HORIZONTAL,
                    Index = 0,
                    From = 0,
                    To = 2
                },
                new Match()
                {
                    Axis = Match.BoardAxis.VERTICAL,
                    Index = 3,
                    From = 1,
                    To = 4
                }
            };

            Dictionary<int, List<int>> pieces = BoardUtils.GetPiecesFromMatches(matches);

            Assert.IsTrue(pieces.ContainsKey(0));
            Assert.IsTrue(pieces.ContainsKey(1));
            Assert.IsTrue(pieces.ContainsKey(2));
            Assert.AreEqual(0, pieces[0][0]);
            Assert.AreEqual(0, pieces[1][0]);
            Assert.AreEqual(0, pieces[2][0]);
            
            Assert.IsTrue(pieces.ContainsKey(3));
            Assert.AreEqual(1, pieces[3][0]);
            Assert.AreEqual(2, pieces[3][1]);
            Assert.AreEqual(3, pieces[3][2]);
            Assert.AreEqual(4, pieces[3][3]);
        }

        [Test]
        public void DoBoardGravity_TwoMatches_ThePiecesMove()
        {
            _board.Initialize();
            var pieces = new Dictionary<int, List<int>>(2)
            {
                {0, new List<int>(6) {1, 2 , 3, 5, 6, 7}}
            };

            var piece1 = _board[0, 0];
            var piece2 = _board[0, 4];
            
            _board.DoBoardGravity(pieces);
            
            Assert.AreEqual(piece1, _board[0, 5]);
            Assert.AreEqual(piece2, _board[0, 7]);
        }
    }
}