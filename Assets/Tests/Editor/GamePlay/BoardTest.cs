using System.Collections.Generic;
using com.raphael.bejeweled.Gameplay;
using com.raphael.bejeweled.Utils;
using NUnit.Framework;
using UnityEditor;

namespace Tests.Editor.GamePlay
{
    public class BoardTest
    {
        private Board _board;

        [SetUp]
        public void SetUp()
        {
            _board = AssetDatabase.LoadAssetAtPath<Board>("Assets/ScriptableObjects/Board.asset");
        }
        
        [Test]
        public void Initialize_SetAllPiecesWithMatch_AllPiecesAreSet()
        {
            _board.Initialize();

            Assert.IsTrue(_board[0, 0] != _board[1, 0] || _board[0, 0] != _board[2, 0]);
        }
        
        [Test]
        public void RemovePiece_RemoveFirstPiece_PieceIsRemoved()
        {
            _board.Initialize();
            _board.RemovePiece(0 , 0);

            Assert.AreEqual(0, _board[0, 0]);
        }
        
        [Test]
        public void ReplaceVertical_ReplacePieces_PieceAreFlipped()
        {
            _board.Initialize();
            
            _board.ReplaceVerticalPiece(99,0 , 0, 1);

            Assert.AreEqual(0, _board[0, 0]);
            Assert.AreEqual(99, _board[0, 1]);
        }
        
        [Test]
        public void FlipColumn_FlipPieces_PieceAreFlipped()
        {
            _board.Initialize();
            int piece1 = _board[0, 0];
            int piece2 = _board[1, 0];
            
            _board.FlipColumn(0 , 0, 1);

            Assert.AreEqual(piece2, _board[0, 0]);
            Assert.AreEqual(piece1, _board[1, 0]);
        }
        
        [Test]
        public void FlipRow_FlipPieces_PieceAreFlipped()
        {
            _board.Initialize();
            int piece1 = _board[0, 0];
            int piece2 = _board[0, 1];
            
            _board.FlipRow(0 , 0, 1);

            Assert.AreEqual(piece2, _board[0, 0]);
            Assert.AreEqual(piece1, _board[0, 1]);
        }
        
        [Test]
        public void GetRowPieces_RowHaveNoMatches_TheRightRowIsReturned()
        {
            _board.Initialize();

            var matches = new List<Match>();
            _board.CheckHorizontalMatchesNonAlloc(0, 0, matches);
            Assert.AreEqual(0, matches.Count);
        }
    }
}