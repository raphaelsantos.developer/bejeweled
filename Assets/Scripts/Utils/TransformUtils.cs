using System;
using System.Collections;
using com.raphael.bejeweled.Systems;
using UnityEditor;
using UnityEngine;

namespace com.raphael.bejeweled.Utils
{
    public static class TransformUtils
    {
        private static UnityEventSystem _unityEventSystem;

        private static UnityEventSystem UnityEventSystem
        {
            get
            {
                if (_unityEventSystem == null)
                {
                    _unityEventSystem = SystemFactory.Resolve<UnityEventSystem>();
                }

                return _unityEventSystem;
            }
        }
        
        public static void TranslateTo(this Transform from, Vector3 to, float time, Action onFinish = null)
        {
            float totalTime = 0;
            UnityEventSystem.StartCoroutine(DoTransformTween());

            IEnumerator DoTransformTween()
            {
                float distance = Vector3.Distance(from.position, to) / time;
                while (from != null &&  Vector3.Distance(from.position, to) != 0)
                {
                    totalTime += Time.deltaTime;
                     if (totalTime > time) break;

                     from.position = Vector3.MoveTowards(from.position, to, distance * Time.deltaTime);
                    
                    yield return new WaitForEndOfFrame();
                }

                if (from != null)
                {
                    from.position = to;
                    UnityEventSystem.StopCoroutine(DoTransformTween());

                    onFinish?.Invoke();
                }
            }
        }
        
        public static void ScaleTo(this Transform from, Vector3 to, float time, Action onFinish = null)
        {
            float totalTime = 0;
            UnityEventSystem.StartCoroutine(DoScaleTween());
            
            IEnumerator DoScaleTween()
            {
                float distance = Vector3.Distance(from.localScale, to) / time;
                while (from != null &&  Vector3.Distance(from.localScale, to) != 0)
                {
                    totalTime += Time.deltaTime;
                    if (totalTime > time) break;

                    from.localScale = Vector3.MoveTowards(from.localScale, to, distance * Time.deltaTime);
                    
                    yield return new WaitForEndOfFrame();
                }

                if (from != null)
                {
                    from.localScale = to;
                    UnityEventSystem.StopCoroutine(DoScaleTween());

                    onFinish?.Invoke();
                }
            }
        }

        public static void BounceScaleTo(this Transform from, Vector3 to, float time, Action onFinish = null)
        {
            Vector3 originalPosition = from.localPosition;
            Vector3 targetScale = to * 1.5f;
            from.ScaleTo(targetScale, time - .2f, () =>
            {
                if (from != null)
                {
                    from.localPosition = originalPosition;
                    from.ScaleTo(to, .2f, () =>
                    {
                        from.localPosition = originalPosition;
                        onFinish?.Invoke();
                    });
                }
            });
        }

        public static void ScaleTo(this RectTransform from, Vector3 to, float time, Action onFinish = null)
        {
            Vector3 initialPosition = from.localPosition;
            float totalTime = 0;
            UnityEventSystem.StartCoroutine(DoScaleTween());
            
            IEnumerator DoScaleTween()
            {
                float distance = Vector3.Distance(from.localScale, to) / time;
                while (from != null &&  Vector3.Distance(from.localScale, to) != 0)
                {
                    totalTime += Time.deltaTime;
                    if (totalTime > time) break;

                    from.localScale = Vector3.MoveTowards(from.localScale, to, distance * Time.deltaTime);
                    
                    yield return new WaitForEndOfFrame();
                }

                if (from != null)
                {
                    from.localScale = to;
                    UnityEventSystem.StopCoroutine(DoScaleTween());

                    from.localPosition = initialPosition;
                    onFinish?.Invoke();
                }
            }
        }
        
        public static void BounceScaleTo(this RectTransform from, Vector3 to, float time, Action onFinish = null)
        {
            Vector3 originalPosition = from.localPosition;
            Vector3 targetScale = to * 1.5f;
            from.ScaleTo(targetScale, time -.2f, () =>
            {
                if (from != null)
                {
                    from.localPosition = originalPosition;
                    from.ScaleTo(to, .2f, () =>
                    {
                        from.localPosition = originalPosition;
                        onFinish?.Invoke();
                    });
                }
            });
        }
    }
}