using UnityEngine;

namespace com.raphael.bejeweled.Utils
{
    public static class ScreenUtils
    {
        private static Camera _camera;

        private static Camera Camera
        {
            get
            {
                if (_camera == null)
                {
                    _camera = Camera.main;
                }

                return _camera;
            }
        }

        public static Vector2 GetScreenToWorldSize()
        {
            if (Camera == null) return Vector2.zero;

            Vector2 topRightCorner = new Vector2(1, 1);
            Vector2 edgeVector = Camera.ViewportToWorldPoint(topRightCorner);
            return (edgeVector * 2);
        }
        
        public static Vector2 ScreenToWorldPoint(Vector2 position)
        {
            if (Camera == null) return Vector2.zero;

            return _camera.ScreenToWorldPoint(position);
        }
    }
}