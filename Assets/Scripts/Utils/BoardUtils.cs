using System.Collections.Generic;
using com.raphael.bejeweled.Gameplay;
using Random = UnityEngine.Random;

namespace com.raphael.bejeweled.Utils
{
    public static class BoardUtils
    {
        #region Match

        public static void CheckHorizontalMatchesNonAlloc(this Board board, int row, int startFrom, List<Match> matches)
        {
            byte[] pieces = new byte[board.RowSize];
            board.GetRowPiecesNonAlloc(row, pieces);
            CheckForMatchesNonAlloc(pieces, startFrom, row, Match.BoardAxis.HORIZONTAL, matches);
        }
        
        public static void CheckVerticalMatchesNonAlloc(this Board board, int column, int startFrom, List<Match> matches)
        {
            byte[] pieces = board[column];
            CheckForMatchesNonAlloc(pieces, startFrom, column, Match.BoardAxis.VERTICAL, matches);
        }

        private static void CheckForMatchesNonAlloc(byte[] pieces, int startFrom, int index, Match.BoardAxis axis,
            List<Match> matches)
        {
            if (TryFindMatch(pieces[startFrom], startFrom, out Match newMatch))
            {
                newMatch.Index = index;
                newMatch.Axis = axis;
                matches.Add(newMatch);
            }

            if (startFrom >= 3 &&
                (matches.Count == 0 || matches[0].From == startFrom) &&
                TryFindMatch(pieces[startFrom - 1], startFrom - 1, out Match newMath1))
            {
                newMath1.Index = index;
                newMath1.Axis = axis;
                matches.Add(newMath1);
            }

            if (startFrom <= (pieces.Length - 3) &&
                (matches.Count == 0 || matches[0].To == startFrom) &&
                TryFindMatch(pieces[startFrom + 1], startFrom + 1, out Match newMath2))
            {
                newMath2.Index = index;
                newMath2.Axis = axis;
                matches.Add(newMath2);
            }

            bool TryFindMatch(int type, int start, out Match match)
            {
                match = new Match()
                {
                    From = start,
                    To = start
                };

                if (type == 0) return false;


                int indexMultiplier = 0;
                bool searchRight = true;
                bool searchLeft = true;

                do
                {
                    indexMultiplier++;
                    var currentIndex = start + indexMultiplier;

                    if (searchRight)
                    {
                        if (currentIndex < pieces.Length && pieces[currentIndex] == type)
                        {
                            match.To = currentIndex;
                        }
                        else
                        {
                            searchRight = false;
                        }
                    }

                    if (searchLeft)
                    {
                        currentIndex = start - indexMultiplier;

                        if (currentIndex >= 0 && pieces[currentIndex] == type)
                        {
                            match.From = currentIndex;
                        }
                        else
                        {
                            searchLeft = false;
                        }
                    }


                } while (searchRight || searchLeft);

                return match.Size >= 3;
            }
        }

        public static Dictionary<int, List<int>> GetPiecesFromMatches(List<Match> matches)
        {
            var result = new Dictionary<int, List<int>>();
            foreach (Match match in matches)
            {
                if (match.Axis == Match.BoardAxis.HORIZONTAL)
                {
                    GetHorizontalPieces(match.Index, match.From, match.To);
                    continue;
                }
                
                GetVerticalPieces(match.Index, match.From, match.To);
            }

            void GetHorizontalPieces(int row, int from, int to)
            {
                for (int i = from; i <= to; i++)
                {
                    if (!result.TryGetValue(i, out List<int> pieces))
                    {
                        pieces = new List<int>();
                        result[i] = pieces;
                    }
                    
                    if (!pieces.Exists(piece => piece == row)) pieces.Add(row);
                }
            }
            
            void GetVerticalPieces(int column, int from, int to)
            {
                if (!result.TryGetValue(column, out List<int> pieces))
                {
                    pieces = new List<int>();
                    result[column] = pieces;
                }
                
                for (int i = from; i <= to; i++)
                {
                    if (!pieces.Exists(piece => piece == i)) pieces.Add(i);
                }
            }

            foreach (List<int> pieces in result.Values)
            {
                pieces.Sort();
            }

            return result;
        }

        #endregion

        public static void DoBoardGravity(this Board board, Dictionary<int, List<int>> piecesToRemove)
        { 
            int dropAmount = 0;
            
           foreach (int key in piecesToRemove.Keys)
           {
               for (int j = piecesToRemove[key].Count - 1; j >= 0; j--)
               {
                    int index = piecesToRemove[key][j];
                    int previousPiece = j > 0 ? piecesToRemove[key][j - 1] : -20;

                    if (previousPiece >= 0 && previousPiece == index - 1)
                    {
                        dropAmount++;
                        continue;
                    }
                    
                    while (index >= (-dropAmount) && index > previousPiece)
                    {
                        byte piece = 0;
                        
                        if (index <= 0 || board[key, index - 1] == -0)
                        {
                            piece = board.PiecesValues[Random.Range(0, board.PiecesTypes.Length)];
                        }
                        else
                        {
                            piece = board[key, index - 1];
                        }

                        board.ReplaceVerticalPiece(piece, key, index - 1, index + dropAmount);
                        index--;
                    }
               }
               
               dropAmount = 0;
           }
        }
    }
}