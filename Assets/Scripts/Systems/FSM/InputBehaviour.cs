using com.raphael.bejeweled.Gameplay;
using com.raphael.bejeweled.Systems.Event;
using com.raphael.bejeweled.Systems.Input;
using com.raphael.bejeweled.Utils;
using UnityEngine;

namespace com.raphael.bejeweled.Systems.FSM
{
    public class InputBehaviour : StateMachineBehaviour
    {
        [SerializeField]
        private string _fromColumnParameter = null;
        
        [SerializeField]
        private string _fromRowParameter = null;
        
        [SerializeField]
        private string _toColumnParameter = null;
        
        [SerializeField]
        private string _toRowParameter = null;

        [SerializeField]
        private string _processFlipTrigger = null;
        
        [SerializeField]
        private string _finishTrigger = null;
        
        [SerializeField]
        private Board _board = null;

        private InputSystem _inputSystem;
        private EventSystem _eventSystem;
        private Animator _animator;
        private Vector2Int _selectedPiece;

        private bool _isRuningState;
        private bool _levelWasComplete;

        private void Awake()
        {
            _inputSystem = SystemFactory.Resolve<InputSystem>();
            _eventSystem = SystemFactory.Resolve<EventSystem>();
            
            _eventSystem.Register<PieceFlipEvent>(OnPieceFlip);
            _eventSystem.Register<LevelCompleteEvent>(OnLevelComplete);
        }

        private void OnPieceFlip(object obj)
        {
            var pieceFlipEvent = (PieceFlipEvent) obj;
            
            _animator.SetInteger(_fromColumnParameter, pieceFlipEvent.FromColumn);
            _animator.SetInteger(_fromRowParameter, pieceFlipEvent.FromRow);
            _animator.SetInteger(_toColumnParameter, pieceFlipEvent.ToColumn);
            _animator.SetInteger(_toRowParameter, pieceFlipEvent.ToRow);
            _animator.SetTrigger(_processFlipTrigger);
        }

        private void OnDestroy()
        {
            _eventSystem.Unregister<PieceFlipEvent>(OnPieceFlip);
            _eventSystem.Unregister<LevelCompleteEvent>(OnLevelComplete);
            
            _inputSystem.StartInputSystem();
            _inputSystem.OnTouchEnterCallback -= OnTouchEnter;
            _inputSystem.OnTouchEndCallback -= OnTouchEnd;
        }

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _isRuningState = true;
            if (_levelWasComplete)
            {
                OnLevelComplete(null);
            }
            
            _animator = animator;
            
            _selectedPiece = Vector2Int.one * -1;
            _inputSystem.OnTouchEnterCallback += OnTouchEnter;
            
            _inputSystem.StartInputSystem();
        }

        public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _eventSystem.Unregister<PieceFlipEvent>(OnPieceFlip);
            
            _inputSystem.StopInputSystem();
            _inputSystem.OnTouchEnterCallback -= OnTouchEnter;
            _inputSystem.OnTouchEndCallback -= OnTouchEnd;
            
            _isRuningState = false;
        }

        public void Flip(int fromColumn, int fromRow, int toColumn, int toRow)
        {
            int flipColumn = Mathf.Abs(fromColumn - toColumn);
            int flipRow = Mathf.Abs(fromRow - toRow);

            if (flipColumn == 1 && flipRow >= 1 ||
                flipColumn >= 1 && flipRow == 1) return;

            if (flipColumn == 1)
            {
                FlipHorizontal(fromRow, fromColumn, toColumn);
            }
            
            if (flipRow == 1)
            {
                FlipVertical(fromColumn, fromRow, toRow);
            }
            
            _animator.SetInteger(_fromColumnParameter, fromColumn);
            _animator.SetInteger(_fromRowParameter, fromRow);
            _animator.SetInteger(_toColumnParameter, toColumn);
            _animator.SetInteger(_toRowParameter, toRow);
            _animator.SetTrigger(_processFlipTrigger);
        }

        private void FlipHorizontal(int row, int from, int to)
        {
            _board.FlipColumn(row, from, to);
        }
        
        private void FlipVertical(int column, int from, int to)
        {
            _board.FlipRow(column, from, to);
        }

        private void OnTouchEnter()
        {
            _inputSystem.OnTouchEnterCallback -= OnTouchEnter;
            _inputSystem.OnTouchEndCallback += OnTouchEnd;
            
            OnTouch();
        }

        private void OnTouchEnd()
        {
            _inputSystem.OnTouchEnterCallback += OnTouchEnter;
            _inputSystem.OnTouchEndCallback -= OnTouchEnd;
            
            OnTouch();
        }

        private void OnTouch()
        {
            if (_board.GetPieceByPosition(ScreenUtils.ScreenToWorldPoint(_inputSystem.PointPosition), out int column,
                out int row) && (column != _selectedPiece.x || row != _selectedPiece.y))
            {
                var newPiece = new Vector2Int(column, row);
                
                _eventSystem.Dispatch(new PieceSelectEvent() { Column = column, Row = row});
                
                if (_selectedPiece.x != -1)
                {
                    Flip(_selectedPiece.x, _selectedPiece.y, newPiece.x, newPiece.y);
                }

                _selectedPiece = newPiece;
            }
        }
        
        private void OnLevelComplete(object obj)
        {
            _levelWasComplete = true;

            if (_isRuningState)
            {
                _animator.SetTrigger(_finishTrigger);
            }
        }
    }
}