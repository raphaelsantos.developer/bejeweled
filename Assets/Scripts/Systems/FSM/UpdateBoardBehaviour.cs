using System.Collections.Generic;
using com.raphael.bejeweled.Gameplay;
using com.raphael.bejeweled.Systems.Event;
using com.raphael.bejeweled.Utils;
using UnityEngine;

namespace com.raphael.bejeweled.Systems.FSM
{
    public class UpdateBoardBehaviour : StateMachineBehaviour
    {
        [SerializeField]
        private string _processFlipTrigger = null;

        [SerializeField]
        private Board _board = null;

        private Dictionary<int, List<int>> _lastRemovePieces;
        private EventSystem _eventSystem;

        private void Awake()
        {
            _eventSystem = SystemFactory.Resolve<EventSystem>();
            _eventSystem.Register<MatchFoundEvent>(OnMatchFound);
        }

        private void OnDestroy()
        {
            _eventSystem.Unregister<MatchFoundEvent>(OnMatchFound);
        }

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            foreach (KeyValuePair<int, List<int>> pieceToRemove in _lastRemovePieces)
            {
                foreach (int row in pieceToRemove.Value)
                {
                    _board.RemovePiece(pieceToRemove.Key, row);
                }
            }
            
            _board.DoBoardGravity(_lastRemovePieces);
            animator.SetTrigger(_processFlipTrigger);
        }
        
        private void OnMatchFound(object obj)
        {
            _lastRemovePieces = (obj as MatchFoundEvent)?.PiecesToRemove;
        }
    }
}