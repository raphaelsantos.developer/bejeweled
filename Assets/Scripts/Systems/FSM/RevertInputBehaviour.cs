using System;
using com.raphael.bejeweled.Gameplay;
using UnityEngine;

namespace com.raphael.bejeweled.Systems.FSM
{
    public class RevertInputBehaviour : StateMachineBehaviour
    {
        [SerializeField]
        private string _fromColumnParameter = null;
        
        [SerializeField]
        private string _fromRowParameter = null;
        
        [SerializeField]
        private string _toColumnParameter = null;
        
        [SerializeField]
        private string _toRowParameter = null;

        [SerializeField]
        private string _waitForInputParameter = null;
        
        [SerializeField]
        private Board _board = null;
        
        private Animator _animator;

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _animator = animator;
            
            int fromColumn = animator.GetInteger(_fromColumnParameter);
            int fromRow = animator.GetInteger(_fromRowParameter);
            int toColumn = animator.GetInteger(_toColumnParameter);
            int toRow = animator.GetInteger(_toRowParameter);
            
            Flip(fromColumn, fromRow, toColumn, toRow);
        }
        
        private void Flip(int fromColumn, int fromRow, int toColumn, int toRow)
        {
            if (Mathf.Abs(fromColumn - toColumn) == 1)
            {
                FlipHorizontal(fromRow, fromColumn, toColumn);
            }
            
            if (Mathf.Abs(fromRow - toRow) == 1)
            {
                FlipVertical(fromColumn, fromRow, toRow);
            }

            _animator.SetTrigger(_waitForInputParameter);
        }

        private void FlipHorizontal(int row, int from, int to)
        {
            _board.FlipColumn(row, from, to);
        }
        
        private void FlipVertical(int column, int from, int to)
        {
            _board.FlipRow(column, from, to);
        }
    }
}