using System;
using System.Collections.Generic;
using com.raphael.bejeweled.Gameplay;
using com.raphael.bejeweled.Systems.Event;
using com.raphael.bejeweled.Utils;
using UnityEngine;

namespace com.raphael.bejeweled.Systems.FSM
{
    public class FindMatchesBehaviour : StateMachineBehaviour
    {
        [SerializeField] 
        private bool _useUserInput = false;
        
        [SerializeField] 
        private string _fromColumnParameter = null;
        
        [SerializeField]
        private string _fromRowParameter = null;
        
        [SerializeField]
        private string _toColumnParameter = null;
        
        [SerializeField]
        private string _toRowParameter = null;

        [SerializeField]
        private string _waitForInputTrigger = null;
        
        [SerializeField]
        private string _updateBoardTrigger = null;
        
        [SerializeField]
        private string _revertInputTrigger = null;
        
        [SerializeField]
        private Board _board = null;
        
        private Animator _animator;
        private EventSystem _eventSystem;
        private Dictionary<int, List<int>> _lastPiecesToRemove;

        private void Awake()
        {
            _eventSystem = SystemFactory.Resolve<EventSystem>();
            _eventSystem.Register<MatchFoundEvent>(OnMatchFound);
        }

        private void OnDestroy()
        {
            if (_eventSystem != null) _eventSystem.Unregister<MatchFoundEvent>(OnMatchFound);
        }

        private void OnMatchFound(object obj)
        {
            _lastPiecesToRemove = (obj as MatchFoundEvent)?.PiecesToRemove;
        }

        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _animator = animator;

            if (_useUserInput)
            {
                int fromColumn = animator.GetInteger(_fromColumnParameter);
                int fromRow = animator.GetInteger(_fromRowParameter);
                int toColumn = animator.GetInteger(_toColumnParameter);
                int toRow = animator.GetInteger(_toRowParameter);
            
                FindMatchesFromFlip(fromColumn, fromRow, toColumn, toRow);
                return;
            }

            FindMatchesFromRemovedPieces();
        }
        
        private void FindMatchesFromFlip(int fromColumn, int fromRow, int toColumn, int toRow)
        {
            List<Match> matches = new List<Match>();
            
            if (Mathf.Abs(fromColumn - toColumn) == 1)
            {
                FindMatchFromHorizontalFlip(fromRow, fromColumn, toColumn, matches);
            }
            
            if (Mathf.Abs(fromRow - toRow) == 1)
            {
                FindMatchFromVerticalFlip(fromColumn, fromRow, toRow, matches);
            }

            ComputeMatches(matches);
        }

        private void FindMatchesFromRemovedPieces()
        {
            if (_lastPiecesToRemove == null)
            {
                _animator.SetTrigger(_waitForInputTrigger);
                return;
            }
            
            List<Match> matches = new List<Match>();

            foreach (KeyValuePair<int, List<int>> pieceToRemove in _lastPiecesToRemove)
            {
                int size = pieceToRemove.Value[pieceToRemove.Value.Count - 1];
                if (size == _board.ColumnSize - 1)
                {
                    size--;
                }
                
                for (int i = 0; i <= size; i++)
                {
                    FindMatchFromVerticalFlip(pieceToRemove.Key, i, i + 1, matches);
                }
            }
            
            ComputeMatches(matches);
        }

        private void ComputeMatches(List<Match> matches)
        {
            if (matches.Count > 0)
            {
                _eventSystem.Dispatch(new MatchFoundEvent()
                    {Matches = matches, PiecesToRemove = BoardUtils.GetPiecesFromMatches(matches)});
                
                _animator.SetTrigger(_updateBoardTrigger);
                return;
            }

            if (_useUserInput)
            {
                _animator.SetTrigger(_revertInputTrigger);
                return;
            }
            
            _animator.SetTrigger(_waitForInputTrigger);
        }

        private void FindMatchFromHorizontalFlip(int row, int from, int to, List<Match> matches)
        {
            _board.CheckHorizontalMatchesNonAlloc(row, from, matches);
            _board.CheckVerticalMatchesNonAlloc(from, row, matches);
            _board.CheckVerticalMatchesNonAlloc(to, row, matches);
        }
        
        private void FindMatchFromVerticalFlip(int column, int from, int to, List<Match> matches)
        {
            _board.CheckVerticalMatchesNonAlloc(column, from, matches);
            _board.CheckHorizontalMatchesNonAlloc(from, column, matches);
            _board.CheckHorizontalMatchesNonAlloc(to, column, matches);
        }
    }
}