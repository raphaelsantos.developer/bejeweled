using com.raphael.bejeweled.Gameplay;
using UnityEngine;

namespace com.raphael.bejeweled.Systems.FSM
{
    public class StartBehaviour : StateMachineBehaviour
    {
        [SerializeField]
        private string _waitForInputTrigger = null;

        [SerializeField]
        private Board _board = null;
        
        public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
        {
            _board.Initialize();
            
            animator.SetTrigger(_waitForInputTrigger);
        }
    }
}