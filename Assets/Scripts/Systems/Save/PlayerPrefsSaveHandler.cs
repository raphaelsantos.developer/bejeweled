using UnityEditor;
using UnityEngine;

namespace com.raphael.bejeweled.Systems.Save
{
    public class PlayerPrefsSaveHandler : ISaveHandler
    {
        public bool HasKey(string key)
        {
            return PlayerPrefs.HasKey(key);
        }
        
        public void Save(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
        }
        
        public void Save(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
        }

        public int LoadInt(string key)
        {
            return PlayerPrefs.GetInt(key, 0);
        }

        public string LoadString(string key)
        {
            return PlayerPrefs.GetString(key, "");
        }

        public void Clear()
        {
            PlayerPrefs.DeleteAll();
        }

#if UNITY_EDITOR
        [MenuItem("Bejeweled/Clear Data")]
        static void DoSomething()
        {
            PlayerPrefs.DeleteAll();
        }
#endif
    }
}