namespace com.raphael.bejeweled.Systems.Save
{
    public interface ISaveHandler
    {
        bool HasKey(string key);
        
        void Save(string key, int value);
        
        void Save(string key, string value);

        int LoadInt(string key);
        
        string LoadString(string key);

        void Clear();
    }
}