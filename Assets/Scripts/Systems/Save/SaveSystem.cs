namespace com.raphael.bejeweled.Systems.Save
{
    public class SaveSystem
    {
        private const string _saveKey = "raphael.com.bejeweled";

        private readonly ISaveHandler _saveHandler;

        public SaveSystem()
        {
            _saveHandler = new PlayerPrefsSaveHandler();
        }

        public bool HasKey(string key)
        {
            return _saveHandler.HasKey(key);
        }

        public void Save(string key, int value)
        {
            _saveHandler.Save($"{_saveKey}_{key}", value);
        }
        
        public void Save(string key, string value)
        {
            _saveHandler.Save($"{_saveKey}_{key}", value);
        }
        
        public int LoadInt(string key)
        {
            return _saveHandler.LoadInt($"{_saveKey}_{key}");
        }
        
        public string LoadString(string key)
        {
            return _saveHandler.LoadString($"{_saveKey}_{key}");
        }

        public void Clear()
        {
            _saveHandler.Clear();
        }
    }
}