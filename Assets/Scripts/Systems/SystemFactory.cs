using System;
using System.Collections.Generic;
using com.raphael.bejeweled.Systems.Event;
using com.raphael.bejeweled.Systems.Input;
using com.raphael.bejeweled.Systems.Save;
using UnityEngine;

namespace com.raphael.bejeweled.Systems
{
    public static class SystemFactory
    {
        private static readonly Dictionary<Type, object> _systems;

        static SystemFactory()
        {
            var unityEventSystem = new UnityEventSystem();
            var saveSystem = new SaveSystem();
            
            _systems = new Dictionary<Type, object>()
            {
                {typeof(SaveSystem), saveSystem},
                {typeof(PoolingSystem), new PoolingSystem()},
                {typeof(UnityEventSystem), unityEventSystem},
                {typeof(ActionQueueSystem), new ActionQueueSystem()},
                {typeof(InputSystem), new InputSystem(unityEventSystem)},
                {typeof(AudioSystem), new AudioSystem(saveSystem)},
                {typeof(EventSystem), new EventSystem()}
            };
        }
        
        public static T Resolve<T>()
        {
            Type type = typeof(T);
            if (!_systems.TryGetValue(type, out object result))
            {
                Debug.Log($"[SystemFactory] The system of type {type.ToString()} wasn't registered in the factory.");
            }

            return (T) result;
        }
    }
}