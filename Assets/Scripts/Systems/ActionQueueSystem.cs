using System;
using System.Collections.Generic;

namespace com.raphael.bejeweled.Systems
{
    public class ActionQueueSystem
    {
        private class QueueAction
        {
            public Action<Action> Event;
            public int Group;
        }
        
        private readonly Queue<QueueAction> _queue;
        private bool _isWaitingForEvent;

        public ActionQueueSystem()
        {
            _queue = new Queue<QueueAction>(20);
        }

        public void Reset()
        {
            _queue.Clear();
            _isWaitingForEvent = false;
        }

        public void Enqueue(Action<Action> action, int group = -1)
        {
            _queue.Enqueue(new QueueAction()
            {
                Event = action,
                Group = group
            });

            if (!_isWaitingForEvent)
            {
                Process();
            }
        }

        private void Process()
        {
            QueueAction currentAction = _queue.Dequeue();
            QueueAction nextAction = _queue.Count == 0 ? null : _queue.Peek();

            _isWaitingForEvent = true;
            bool goNextEvent = nextAction != null && nextAction.Group != -1 && nextAction.Group == currentAction.Group;
            
            currentAction.Event.Invoke(ProcessNextEvent);

            if (goNextEvent)
            {
                  Process();
            }

            void ProcessNextEvent()
            {
                _isWaitingForEvent = false;
                if (!goNextEvent && _queue.Count > 0)
                {
                    Process();
                }
            }
        }
    }
}