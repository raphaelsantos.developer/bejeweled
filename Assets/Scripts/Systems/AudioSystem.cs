using System;
using com.raphael.bejeweled.Systems.Save;
using UnityEngine;
using Object = UnityEngine.Object;

namespace com.raphael.bejeweled.Systems
{
    public class AudioSystem
    {
        private readonly AudioSource _audioSource;

        private bool _isEnabled;
        public bool IsEnabled => _isEnabled;

        private float _volume = .5f;
        public float Volume => _volume;

        public Action<bool> OnAudioEnabledChange;

        private readonly SaveSystem _saveSystem;

        public AudioSystem(SaveSystem saveSystem)
        {
            _saveSystem = saveSystem;
            GameObject target = new GameObject("AudioSystem", typeof(AudioSource));
            _audioSource = target.GetComponent<AudioSource>();
            
#if  !UNITY_TESTS
            if (!Application.isBatchMode) Object.DontDestroyOnLoad(target);
#endif

            _isEnabled = !_saveSystem.HasKey("audio") || _saveSystem.LoadInt("audio") == 1;
            _audioSource.volume = _volume;
        }

        public void PlayOneShot(AudioClip audioClip)
        {
            if (!_isEnabled) return;
            
            _audioSource.PlayOneShot(audioClip, 5);
        }

        public void Play(AudioClip audioClip)
        {
            _audioSource.clip = audioClip;
            _audioSource.Play();
        }

        public void SetAudioVolume(float volume)
        {
            _volume = volume;
            _audioSource.volume = _volume;
        }
        
        public void SetAudioEnabled(bool isEnabled)
        {
            _isEnabled = isEnabled;

            _audioSource.mute = !_isEnabled;
            _saveSystem.Save("audio", Convert.ToInt32(_isEnabled));
            
            OnAudioEnabledChange?.Invoke(_isEnabled);
        }
    }
}