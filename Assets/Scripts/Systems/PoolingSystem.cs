using System;
using System.Collections.Generic;
using UnityEngine;
using Object = UnityEngine.Object;

namespace com.raphael.bejeweled.Systems
{
    public class PoolingSystem
    {
        public Action<GameObject> OnInstantiate;
        public Action<GameObject> OnDestroy;
        
        private readonly List<GameObject> _objects;

        public PoolingSystem()
        {
            _objects = new List<GameObject>();
        }
        
        public GameObject Instantiate(GameObject gameObject, Vector3 position, Quaternion rotation, Transform parent)
        {
            GameObject result = null;
            
            if (_objects.Count > 0)
            {
                result = _objects[0];
                _objects.RemoveAt(0);

                Transform transform = result.GetComponent<Transform>();
                transform.position = position;
                transform.rotation = rotation;
                transform.parent = parent;
            }
            else
            {
                result = Object.Instantiate(gameObject, position, rotation, parent);
            }
            
            result.SetActive(true);
            
            OnInstantiate?.Invoke(result);
            
            return result;
        }

        public GameObject Instantiate(GameObject gameObject)
        {
            return Instantiate(gameObject, Vector3.zero, Quaternion.identity, null);
        }
        
        public void Destroy(GameObject gameObject)
        {
            gameObject.SetActive(false);
            _objects.Add(gameObject);
            
            OnDestroy?.Invoke(gameObject);
        }
    }
}