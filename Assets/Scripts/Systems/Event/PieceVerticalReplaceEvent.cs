namespace com.raphael.bejeweled.Systems.Event
{
    public class PieceVerticalReplaceEvent : IEvent
    {
        public int Piece;
        public int Column;
        public int FromRow;
        public int ToRow;
    }
}