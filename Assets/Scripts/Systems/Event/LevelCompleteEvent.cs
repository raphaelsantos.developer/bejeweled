using com.raphael.bejeweled.Gameplay;

namespace com.raphael.bejeweled.Systems.Event
{
    public class LevelCompleteEvent : IEvent
    {
        public LevelConfig.Level Level;
    }
}