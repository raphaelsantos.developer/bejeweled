using System;
using System.Collections.Generic;
using com.raphael.bejeweled.Systems.Event;

namespace com.raphael.bejeweled.Systems.Event
{
    public class EventSystem
    {
        private readonly Dictionary<Type, Action<object>> _listeners;

        public EventSystem()
        {
            _listeners = new Dictionary<Type, Action<object>>();
        }
        

        public void Register<T>(Action<object> listener) where T : IEvent
        {
            Type type = typeof(T);
            
            if (!_listeners.TryGetValue(type, out Action<object> dispatcher))
            {
                _listeners[type] = listener;
                return;
            }
            
            _listeners[type] += listener;
        }
        
        public void Unregister<T>(Action<object> listener) where T : IEvent
        {
            Type type = typeof(T);
            
            if (!_listeners.ContainsKey(type)) return;
            
            _listeners[type] -= listener;
        }
        
        public void Dispatch<T>(T context) where T : IEvent
        {
            if (_listeners.TryGetValue(typeof(T), out Action<object> listener))
            {
                listener?.Invoke(context);
            }
        }
    }
}