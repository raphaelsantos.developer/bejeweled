namespace com.raphael.bejeweled.Systems.Event
{
    public class PieceRemoveEvent : IEvent
    {
        public int Column;
        public int Row;
    }
}