using System.Collections.Generic;
using com.raphael.bejeweled.Gameplay;

namespace com.raphael.bejeweled.Systems.Event
{
    public class MatchFoundEvent : IEvent
    {
        public List<Match> Matches;
        public Dictionary<int, List<int>> PiecesToRemove;
    }
}