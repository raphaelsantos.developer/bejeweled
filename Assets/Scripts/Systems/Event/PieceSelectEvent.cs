namespace com.raphael.bejeweled.Systems.Event
{
    public class PieceSelectEvent : IEvent
    {
        public int Column;
        public int Row;
    }
}