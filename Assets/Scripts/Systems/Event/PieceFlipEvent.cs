namespace com.raphael.bejeweled.Systems.Event
{
    public class PieceFlipEvent : IEvent
    {
        public int FromColumn;
        public int FromRow;
        public int ToColumn;
        public int ToRow;
    }
}