using System;
using System.Collections;
using UnityEngine;
using Object = UnityEngine.Object;

namespace com.raphael.bejeweled.Systems
{
    public class UnityEventSystem
    {
        private readonly UnityEventDispatcher _dispatcher;

        public Action OnUpdate
        {
            get => _dispatcher.OnUpdate;
            set => _dispatcher.OnUpdate = value;
        }
        
        public UnityEventSystem()
        {
            var dispatcherObject = new GameObject("UnityEventSystem");
            _dispatcher = dispatcherObject.AddComponent<UnityEventDispatcher>();
#if !UNITY_TESTS
            if (!Application.isBatchMode) Object.DontDestroyOnLoad(dispatcherObject); 
#endif
        }

        public void CallLater(float delay, Action action)
        {
            _dispatcher.CallLater(delay, action);
        }
        
        public void StartCoroutine(IEnumerator coroutine)
        {
            _dispatcher.StartCoroutine(coroutine);
        }
        
        public void StopCoroutine(IEnumerator coroutine)
        {
            _dispatcher.StopCoroutine(coroutine);
        }
    }

    public class UnityEventDispatcher : MonoBehaviour
    {
        public Action OnUpdate;

        private void Update()
        {
            OnUpdate?.Invoke();
        }

        public void CallLater(float delay, Action action)
        {
            if (delay < 0)
            {
                Debug.LogError("[UnityEventSystem] The `delay` can't be less than zero.");
                return;
            }

            StartCoroutine(CallAfterDelay());
            
            IEnumerator CallAfterDelay()
            {
                yield return new WaitForSeconds(delay);
                
                action.Invoke();
                
                StopCoroutine(CallAfterDelay());
            }
        }
    }
}