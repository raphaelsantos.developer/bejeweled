﻿using UnityEngine;

namespace com.raphael.bejeweled.Systems.Input.PlataformHandlers
{
    public interface IInputPlatformHandler
    {
        bool IsPointing();
        bool IsPointingEnter();
        bool IsPointingEnd();
        Vector2 GetPointPosition();
        Vector2 GetCenterPointPosition();

    }
}
