﻿using UnityEngine;
using UnityInput = UnityEngine.Input;

namespace com.raphael.bejeweled.Systems.Input.PlataformHandlers
{
    public class DesktopInputPlatformHandler : IInputPlatformHandler
    {
        public Vector2 GetCenterPointPosition()
        {
            return new Vector2(Screen.width / 2, Screen.height / 2);
        }

        public bool IsPointing()
        {
            return UnityInput.GetMouseButton(0);
        }

        public bool IsPointingEnter()
        {
            return UnityInput.GetMouseButtonDown(0);
        }

        public bool IsPointingEnd()
        {
            return UnityInput.GetMouseButtonUp(0);
        }

        public Vector2 GetPointPosition()
        {
            return UnityInput.mousePosition;
        }
    }
}
