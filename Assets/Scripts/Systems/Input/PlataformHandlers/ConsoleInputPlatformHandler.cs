﻿using UnityEngine;

namespace com.raphael.bejeweled.Systems.Input.PlataformHandlers
{
    public class ConsoleInputPlatformHandler : IInputPlatformHandler
    {
        public Vector2 GetCenterPointPosition()
        {
            return new Vector2(Screen.width / 2, Screen.height);
        }

        public bool IsPointing()
        {
            throw new System.NotImplementedException();
        }

        public bool IsPointingEnter()
        {
            throw new System.NotImplementedException();
        }

        public bool IsPointingEnd()
        {
            throw new System.NotImplementedException();
        }

        public Vector2 GetPointPosition()
        {
            throw new System.NotImplementedException();
        }
    }
}
