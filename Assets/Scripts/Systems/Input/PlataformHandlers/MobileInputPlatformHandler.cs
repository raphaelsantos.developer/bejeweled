﻿using UnityEngine;
using UnityInput = UnityEngine.Input;

namespace com.raphael.bejeweled.Systems.Input.PlataformHandlers
{
    public class MobileInputPlatformHandler : IInputPlatformHandler
    {
        private Vector2 _lastPosition;
        
        public Vector2 GetCenterPointPosition()
        {
            return new Vector2(Screen.width / 2, Screen.height);
        }

        public bool IsPointing()
        {
            _lastPosition = GetPointPosition();
            return UnityInput.touchCount > 0;
        }

        public bool IsPointingEnter()
        {
            return UnityInput.touchCount > 0 &&
                   UnityInput.GetTouch(0).phase == TouchPhase.Began;
        }

        public bool IsPointingEnd()
        {
            return UnityInput.touchCount == 0 ||
                   UnityInput.GetTouch(0).phase == TouchPhase.Ended;
        }

        public Vector2 GetPointPosition()
        {
            return UnityInput.touchCount > 0 ? UnityInput.GetTouch(0).position : _lastPosition;
        }
    }
}
