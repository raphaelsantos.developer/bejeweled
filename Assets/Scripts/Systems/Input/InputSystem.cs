using System;
using com.raphael.bejeweled.Systems.Input.PlataformHandlers;
using UnityEngine;

namespace com.raphael.bejeweled.Systems.Input
{
    public class InputSystem
    {
        private readonly IInputPlatformHandler _inputPlatformHandler;
        private readonly UnityEventSystem _unityEventSystem;
        private bool _isStarted = false;
        public bool IsInteractable = true;

        public Vector3 PointPosition => _inputPlatformHandler.GetPointPosition();
        public Vector3 CenterPointPosition => _inputPlatformHandler.GetCenterPointPosition();

        public Action OnTouchEnterCallback;
        public Action OnTouchCallback;
        public Action OnTouchEndCallback;

        public InputSystem(UnityEventSystem eventSystem)
        {
            _unityEventSystem = eventSystem;
            if (Application.isMobilePlatform)
            {
                _inputPlatformHandler = new MobileInputPlatformHandler();
            }
            else if (Application.isConsolePlatform)
            {
                _inputPlatformHandler = new ConsoleInputPlatformHandler();
            }
            else 
            {
                _inputPlatformHandler = new DesktopInputPlatformHandler();
            }
        }
        
        ~InputSystem()
        {
            if (_isStarted && _unityEventSystem != null) _unityEventSystem.OnUpdate -= OnUpdate;
        }

        private void OnUpdate()
        {
            if (!_isStarted || !IsInteractable) return;
            
            if (_inputPlatformHandler.IsPointingEnter())
            {
                OnTouchEnterCallback?.Invoke();
            }
            else if (_inputPlatformHandler.IsPointing())
            {
                OnTouchCallback?.Invoke();
            }
            else if (_inputPlatformHandler.IsPointingEnd())
            {
                OnTouchEndCallback?.Invoke();
            }
        }

        public void StartInputSystem()
        {
            if (_isStarted) return;

            _isStarted = true;
            _unityEventSystem.OnUpdate += OnUpdate;
        }
        
        public void StopInputSystem()
        {
            if (!_isStarted) return;

            _isStarted = false;
            _unityEventSystem.OnUpdate -= OnUpdate;
        }
    }
}