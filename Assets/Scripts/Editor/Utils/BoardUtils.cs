using System.Text;
using com.raphael.bejeweled.Gameplay;

namespace Editor.Utils
{
    public static class BoardUtils
    {
        public static string Print(this Board board)
        {
            if (board == null) return "";
            
            var boardText = new StringBuilder();

            boardText.AppendLine("-----------------");

            for (int i = 0; i < board.RowSize; i++)
            {
                if (board[i] == null) continue;
                
                boardText.Append("|");
                for (int j = 0; j < board.ColumnSize; j++)
                {
                    boardText.Append($"{board[j, i]}|");
                }

                boardText.Append("\n");
            }

            boardText.Append("-----------------");
            return boardText.ToString();
        }
    }
}