using com.raphael.bejeweled.Gameplay;
using Editor.Utils;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Editor.GamePlay
{
    [CustomEditor(typeof(Board))]
    public class BoardEditor : UnityEditor.Editor
    {
        private Vector2Int _from;
        private Vector2Int _to;
        private Board _board;

        private SerializedProperty _columnSize;
        private SerializedProperty _rowSize;
        private SerializedProperty _piecesTypes;
        private SerializedProperty _piecePrefab;
        private SerializedProperty _distanceFromTop;
        private SerializedProperty _distanceFromSides;

        private ReorderableList _piecesTypeList;
        
        private void OnEnable()
        {
            _columnSize = serializedObject.FindProperty("_columnSize");
            _rowSize = serializedObject.FindProperty("_rowSize");
            _piecesTypes = serializedObject.FindProperty("_piecesTypes");
            _piecePrefab = serializedObject.FindProperty("_piecePrefab");
            _distanceFromTop = serializedObject.FindProperty("_distanceFromTop");
            _distanceFromSides = serializedObject.FindProperty("_distanceFromSides");

            _piecesTypes.arraySize = 5;
            _piecesTypeList = new ReorderableList(serializedObject, _piecesTypes, false, true, false, false)
            {
                drawHeaderCallback = rect => EditorGUI.LabelField(rect, "Pieces Types"),
                drawElementCallback = DrawPieceType
            };

            _board = (Board) target;
            SceneView.duringSceneGui += OnSceneGUI;
        }

        protected void OnDestroy()
        {
            SceneView.duringSceneGui -= OnSceneGUI;
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(_columnSize);
            EditorGUILayout.PropertyField(_rowSize);

            EditorGUILayout.PropertyField(_piecePrefab);
            EditorGUILayout.PropertyField(_distanceFromTop);
            EditorGUILayout.PropertyField(_distanceFromSides);
            
            _piecesTypeList.DoLayoutList();
            
            serializedObject.ApplyModifiedProperties();
            
            if (!Application.isPlaying || !_board.IsInitialize) return;
            
            GUIStyle label = new GUIStyle(EditorStyles.label);
            label.fontSize += 5;
            GUILayout.Label(_board.Print(), label);
            
            if (!Application.isPlaying || _board == null || !_board.IsInitialize) return;

            EditorGUILayout.Separator();

            _from = EditorGUILayout.Vector2IntField("From", _from);
            _to = EditorGUILayout.Vector2IntField("To", _to);

            if (GUILayout.Button("Split"))
            {
                int flipColumn = Mathf.Abs(_from.x - _to.x);
                int flipRow = Mathf.Abs(_from.y - _to.y);

                if (flipColumn == 1 && flipRow >= 1 ||
                    flipColumn >= 1 && flipRow == 1) return;

                if (flipColumn == 1)
                {
                    _board.FlipRow(_from.x, _from.y, _to.y);
                }
            
                if (flipRow == 1)
                {
                    _board.FlipColumn(_from.y, _from.x, _to.x);
                }
            }
        }

        private void OnSceneGUI(SceneView view)
        {
            if (_board == null) return;
            
            for (int i = 0; i < _rowSize.intValue; i++)
            {
                for (int j = 0; j < _columnSize.intValue; j++)
                {
                    Vector2 position = _board.GetPiecePosition(j, i);
                    
                    Handles.DrawWireCube(position, Vector2.one / 2);
                }   
            }
        }

        private void DrawPieceType(Rect rect, int index, bool isActive, bool isFocused)
        {
            SerializedProperty pieceType = _piecesTypes.GetArrayElementAtIndex(index);

            EditorGUI.PropertyField(rect, pieceType);
        }
    }
}