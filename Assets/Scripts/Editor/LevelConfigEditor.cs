using com.raphael.bejeweled.Gameplay;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace Editor
{
    [CustomEditor(typeof(LevelConfig))]
    public class LevelConfigEditor : UnityEditor.Editor
    {
        private SerializedProperty _currentLevel;
        private SerializedProperty _levels;

        private ReorderableList _levelsList;

        private void OnEnable()
        {
            _currentLevel = serializedObject.FindProperty("_currentLevel");
            _levels = serializedObject.FindProperty("_levels");
            
            _levelsList = new ReorderableList(serializedObject, _levels, false, true, true, true)
            {
                drawHeaderCallback = rect => EditorGUI.LabelField(rect, "Levels"),
                drawElementCallback = DrawLevel,
                elementHeight = (EditorGUIUtility.singleLineHeight * 3) + 6
            };
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();

            EditorGUILayout.PropertyField(_currentLevel);
            
            EditorGUILayout.Space();
            
            _levelsList.DoLayoutList();

            serializedObject.ApplyModifiedProperties();
        }
        
        private void DrawLevel(Rect rect, int index, bool isActive, bool isFocused)
        {
            SerializedProperty level = _levels.GetArrayElementAtIndex(index);
            SerializedProperty _scoreEachPiece = level.FindPropertyRelative("_scoreEachPiece");
            SerializedProperty _scoreToWin = level.FindPropertyRelative("_scoreToWin");
            SerializedProperty _movesToThreeStars = level.FindPropertyRelative("_movesToThreeStars");
            SerializedProperty _movesToTwoStars = level.FindPropertyRelative("_movesToTwoStars");

            Rect levelRect = new Rect(rect)
            {
                height = EditorGUIUtility.singleLineHeight
            };
            _scoreEachPiece.intValue = EditorGUI.IntSlider(levelRect, "Piece Score", _scoreEachPiece.intValue, 0, 50);

            levelRect.y += levelRect.height;
            _scoreToWin.floatValue = EditorGUI.Slider(levelRect, "Total Score Win", _scoreToWin.floatValue, 0, 100000);

            Color color = GUI.color;

            if (_movesToThreeStars.intValue >= _movesToTwoStars.intValue)
            {
                GUI.color = Color.red;
            }
            
            levelRect.y += levelRect.height;
            levelRect.width = (levelRect.width / 3) - 10;
            EditorGUI.LabelField(levelRect, "Moves to win: 2 =");
            
            levelRect.x += levelRect.width;
            _movesToTwoStars.intValue =
                EditorGUI.IntField(levelRect, GUIContent.none,  _movesToTwoStars.intValue);
            
            levelRect.x += levelRect.width + 5;
            EditorGUI.LabelField(new Rect(levelRect) { width =  25 }, "3 = ");
            
            levelRect.x += 25;
            _movesToThreeStars.intValue =
                EditorGUI.IntField(levelRect, GUIContent.none, _movesToThreeStars.intValue);

            GUI.color = color;
        }
    }
}