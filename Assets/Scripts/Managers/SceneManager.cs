using System;
using com.raphael.bejeweled.Gameplay.Variables;
using com.raphael.bejeweled.Systems;
using com.raphael.bejeweled.Systems.Event;
using UnityEngine;

namespace com.raphael.bejeweled.Gameplay.Managers
{
    public class SceneManager : MonoBehaviour
    {
        [SerializeField]
        private LevelConfig _levelConfig = null;
        
        [SerializeField]
        private FloatVariable _PlayerScore = null;
        
        private PoolingSystem _poolingSystem;
        private EventSystem _eventSystem;

        private LevelConfig.Level _currentLevel;
        private int _fliCount;
        private bool _isLevelComplete;

        private void Start()
        {
            _poolingSystem = SystemFactory.Resolve<PoolingSystem>();
            _eventSystem = SystemFactory.Resolve<EventSystem>();

            _currentLevel = _levelConfig.GetCurrentLevel();
            _isLevelComplete = false;
            _fliCount = 0;
            _PlayerScore.Value = 0;

            _poolingSystem.OnDestroy += UpdateScore;
            _eventSystem.Register<PieceFlipEvent>(OnFlip);
        }

        private void Update()
        {
            if (Application.platform == RuntimePlatform.Android) {
        
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    UnityEngine.SceneManagement.SceneManager.LoadScene(0);
                }
            }
        }

        private void OnDestroy()
        {
            if (_poolingSystem != null) _poolingSystem.OnDestroy -= UpdateScore;
        }

        private void OnFlip(object obj)
        {
            _fliCount++;
        }

        private void UpdateScore(object obj)
        {   
            _PlayerScore.Value += _currentLevel.ScoreEachPiece;

            if (!_isLevelComplete && _PlayerScore.Value >= _currentLevel.ScoreToWin)
            {
                _levelConfig.UpdateLevel(_currentLevel, _PlayerScore.Value, _fliCount);
                
                _isLevelComplete = true;
                
                _eventSystem.Dispatch(new LevelCompleteEvent() { Level = _currentLevel});
            }
        }
    }
}