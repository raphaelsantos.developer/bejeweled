using UnityEngine;

namespace com.raphael.bejeweled.Gameplay.Managers
{
    public class MenuManager : MonoBehaviour
    {
        private void Update()
        {
            if (Application.platform == RuntimePlatform.Android) 
            {
                if (Input.GetKeyDown(KeyCode.Escape)) 
                {
                    Application.Quit();
                }
            }
        }
    }
}