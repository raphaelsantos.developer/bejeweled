using UnityEngine;

namespace com.raphael.bejeweled.Gameplay
{
    public class Match
    {
        public enum BoardAxis
        {
            HORIZONTAL = 0,
            VERTICAL = 1
        }

        public int Index;
        
        public BoardAxis Axis;
        
        public int From;

        public int To;

        public int Size => Mathf.Abs(From - To) + 1;
    }
}