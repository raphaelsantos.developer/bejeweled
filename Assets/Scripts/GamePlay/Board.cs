﻿using System;
using com.raphael.bejeweled.Systems;
using com.raphael.bejeweled.Systems.Event;
using com.raphael.bejeweled.Utils;
using UnityEngine;
using Random = UnityEngine.Random;

namespace com.raphael.bejeweled.Gameplay
{
    [CreateAssetMenu(fileName = "Board", menuName = "Bejeweled/Board")]
    public class Board : ScriptableObject, ISerializationCallbackReceiver
    {
        [NonSerialized]
        private bool _isInitialize = false;
        public bool IsInitialize => _isInitialize;
        
        [SerializeField]
        private int _columnSize = 0;
        public int ColumnSize => _columnSize;

        [SerializeField]
        private int _rowSize = 0;
        public int RowSize => _rowSize;

        [SerializeField]
        private Sprite[] _piecesTypes = null;
        public Sprite[] PiecesTypes => _piecesTypes;
        
        #region UI
        
        [SerializeField]
        private GameObject _piecePrefab = null;
        public GameObject PiecesPrefab => _piecePrefab;
        
        [SerializeField]
        private float _distanceFromTop = 0;
        
        [SerializeField]
        private float _distanceFromSides = 0;
        
        #region Screen

        private float _pieceSize;
        public float PieceSize
        {
            get
            {
                if (_pieceSize == 0)
                {
                    Vector2 screenSize = ScreenUtils.GetScreenToWorldSize();
                    _pieceSize = ((screenSize.x - (_distanceFromSides * 2)) / _columnSize);
                }

                return _pieceSize;
            }
        }

        private Vector2 _topLeftCorner;
        private Vector2 TopLeftCorner
        {
            get
            {
                if (_topLeftCorner == Vector2.zero)
                {
                    Vector2 screenSize = ScreenUtils.GetScreenToWorldSize();
                    _topLeftCorner = new Vector2(
                        (-screenSize.x / 2) + (PieceSize / 2) + _distanceFromSides,
                        (screenSize.y / 2) - (PieceSize / 2) - _distanceFromTop
                    );
                }

                return _topLeftCorner;
            }
        }
        
        public Vector3 GetPiecePosition(int column, int row)
        {
            return TopLeftCorner + new Vector2(column * PieceSize, row * - PieceSize);
        }
        
        public bool GetPieceByPosition(Vector2 position, out int column, out int row)
        {
            column = Mathf.RoundToInt(((position.x - TopLeftCorner.x) / PieceSize));
            row =  Mathf.RoundToInt(-(position.y - TopLeftCorner.y) / PieceSize);
            
            return column >= 0 && column < _columnSize && row >= 0 && row <_rowSize;
        }

        #endregion

        #endregion
        
        private byte[][] _pieces;
        public readonly byte[] PiecesValues = new byte[5] {1, 2, 3, 4, 5};

        public byte[] this[int column] => _pieces[column];
        public byte this[int column, int row] => _pieces[column][row];

        private EventSystem _eventSystem;

        public void Initialize()
        {
            _eventSystem = SystemFactory.Resolve<EventSystem>();
            _pieces = new byte[RowSize][];

            for (int i = 0; i < RowSize; i++)
            {
                for (int j = 0; j < ColumnSize; j++)
                {
                    if (_pieces[j] == null)
                    {
                        _pieces[j] = new byte[RowSize];
                    }

                    int index = j + (i * ColumnSize);
                    _pieces[j][i] = PiecesValues[Random.Range(0, _piecesTypes.Length)];

                    if (CheckForUndesiredMatches(j, i))
                    {
                        j--;
                    }
                }
            }

            bool CheckForUndesiredMatches(int column, int row)
            {
                if (column >= 2)
                {
                    if (_pieces[column][row] == _pieces[column - 1][row] &&
                        _pieces[column][row] == _pieces[column - 2][row])
                    {
                        return true;
                    }
                }

                if (row >= 2)
                {
                    if (_pieces[column][row] == _pieces[column ][row - 1] &&
                        _pieces[column][row] == _pieces[column][row - 2])
                    {
                        return true;
                    }
                }

                return false;
            }

            _isInitialize = true;
            _eventSystem.Dispatch(new BoardInitializeEvent());
        }

        public void RemovePiece(int column, int row)
        {
            if (_pieces[column][row] == 0) return;
            
            _pieces[column][row] = 0;
            _eventSystem.Dispatch(new PieceRemoveEvent() { Column = column, Row = row});
        }
        
        public void ReplaceVerticalPiece(byte piece, int column, int from, int to)
        {
            if (from >= 0) _pieces[column][from] = 0;
            _pieces[column][to] = piece;
            _eventSystem.Dispatch(new PieceVerticalReplaceEvent()
            {
                Piece = piece,
                Column = column,
                FromRow = from,
                ToRow = to
            });
        }

        public void FlipColumn(int row, int from, int to)
        {
            byte pieceFrom = _pieces[from][row];
            byte pieceTo = _pieces[to][row];

            _pieces[to][row] = pieceFrom;
            _pieces[from][row] = pieceTo;
            
            _eventSystem.Dispatch(new PieceFlipEvent()
            {
                FromColumn = from,
                FromRow = row,
                ToColumn = to,
                ToRow = row
            });
        }
        
        public void FlipRow(int column, int from, int to)
        {
            byte pieceFrom = _pieces[column][from];
            byte pieceTo = _pieces[column][to];

            _pieces[column][to] = pieceFrom;
            _pieces[column][from] = pieceTo;
            
            _eventSystem.Dispatch(new PieceFlipEvent()
            {
                FromColumn = column,
                FromRow = from,
                ToColumn = column,
                ToRow = to
            });
        }
        
        public void GetRowPiecesNonAlloc(int row, byte[] pieces)
        {
            if (row >= RowSize)
            {
                Debug.LogError("[Board] The row it's out of bounds.");
                return;
            };

            for (int i = 0; i < ColumnSize; i++)
            { 
                pieces[i] = _pieces[i][row];
            }
        }
        
        public void OnBeforeSerialize()
        {
            //
        }

        public void OnAfterDeserialize()
        {
            _isInitialize = false;
            _pieceSize = 0;
            _topLeftCorner = Vector2.zero;
            _pieces = null;
        }
    }
}


