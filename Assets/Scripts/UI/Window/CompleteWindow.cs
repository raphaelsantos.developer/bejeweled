using com.raphael.bejeweled.Systems;
using com.raphael.bejeweled.Systems.Event;
using com.raphael.bejeweled.Utils;
using UnityEngine;

namespace com.raphael.bejeweled.UI.Window
{
    public class CompleteWindow : MonoBehaviour
    {
        [SerializeField] 
        private RectTransform _window = null;
        
        [SerializeField]
        private RectTransform Start1 = null;
        
        [SerializeField]
        private RectTransform Start2 = null;
        
        [SerializeField]
        private RectTransform Start3 = null;
        
        [SerializeField]
        private AudioClip _windowAudio = null;
        [SerializeField]
        private AudioClip _starAudio = null;
        
        private EventSystem _eventSystem;
        private AudioSystem _audioSystem;
        
        private void Start()
        {
            _eventSystem = SystemFactory.Resolve<EventSystem>();
            _audioSystem = SystemFactory.Resolve<AudioSystem>();
            
            _eventSystem.Register<LevelCompleteEvent>(OnLevelComplete);

            gameObject.SetActive(false);
        }

        private void OnDestroy()
        {
            if (_eventSystem == null) return;
            _eventSystem.Unregister<LevelCompleteEvent>(OnLevelComplete);
        }

        private void OnLevelComplete(object obj)
        {
            var completeEvent = (LevelCompleteEvent) obj;
            
            gameObject.SetActive(true);
            
            _audioSystem.PlayOneShot(_windowAudio);
            _window.BounceScaleTo(Vector3.one, .5f, () =>
            {
                _audioSystem.PlayOneShot(_starAudio);
                Start1.BounceScaleTo(Vector3.one, .6f, () =>
                {
                    if (completeEvent.Level.Stars >= 2)
                    {
                        _audioSystem.PlayOneShot(_starAudio);
                        Start2.BounceScaleTo(Vector3.one, .6f, () =>
                        {
                            if (completeEvent.Level.Stars >= 3)
                            {
                                _audioSystem.PlayOneShot(_starAudio);
                                Start3.BounceScaleTo(Vector3.one, .6f);
                            }
                        });
                    }
                });
            });
        }
    }
}