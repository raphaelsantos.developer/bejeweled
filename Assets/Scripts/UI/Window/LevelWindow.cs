using com.raphael.bejeweled.Gameplay;
using UnityEngine;

namespace com.raphael.bejeweled.UI.Window
{
    public class LevelWindow : MonoBehaviour
    {
        [SerializeField]
        private GameObject _levelButton = null;
        
        [SerializeField]
        private LevelConfig _levelConfig = null;
        
        [SerializeField]
        private Transform _scrollContent = null;

        private void Start()
        {
            for (int i = 0; i < _levelConfig.Levels.Count; i++)
            {
                Instantiate(_levelButton, _scrollContent);
            }
        }
    }
}