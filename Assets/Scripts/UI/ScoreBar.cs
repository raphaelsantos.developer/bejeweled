using System;
using com.raphael.bejeweled.Gameplay;
using com.raphael.bejeweled.Gameplay.Variables;
using com.raphael.bejeweled.Utils;
using UnityEngine;

namespace com.raphael.bejeweled.UI
{
    public class ScoreBar : MonoBehaviour
    {
        [SerializeField]
        private Transform _bar = null;

        [SerializeField]
        private FloatVariable _score = null;

        [SerializeField]
        private LevelConfig _levelConfig = null;

        private LevelConfig.Level _level;

        private void Start()
        {
            _level = _levelConfig.GetCurrentLevel();
            _score.OnValueUpdate += OnScoreUpdate;
        }

        private void OnDestroy()
        {
            _score.OnValueUpdate -= OnScoreUpdate;
        }

        private void OnScoreUpdate(float obj)
        {
            var localScale = _bar.localScale;
            _bar.ScaleTo(new Vector3(Mathf.Min(_score.Value / _level.ScoreToWin, 1), localScale.y, localScale.z), .1f);
        }
    }
}