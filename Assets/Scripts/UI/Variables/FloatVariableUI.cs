using System;
using com.raphael.bejeweled.Gameplay.Variables;
using UnityEngine;
using UnityEngine.Events;

namespace com.raphael.bejeweled.UI
{
    public class FloatVariableUI : MonoBehaviour
    {
        [SerializeField] 
        private float _changeAmount = 0;
        
        [SerializeField]
        private FloatVariable _variable = null;
        
        [Serializable]
        public class VariableUpdateEvent : UnityEvent<string> {}
        [SerializeField]
        private VariableUpdateEvent OnValueUpdate = null;

        private void Start()
        {
            _variable.OnValueUpdate += OnVariableUpdate;
            
            OnVariableUpdate(_variable.Value);
        }

        private void OnDestroy()
        {
            _variable.OnValueUpdate -= OnVariableUpdate;
        }

        private void OnVariableUpdate(float value)
        {
            OnValueUpdate?.Invoke((value + _changeAmount).ToString());
        }
    }
}