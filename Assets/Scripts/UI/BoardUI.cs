using com.raphael.bejeweled.Gameplay;
using com.raphael.bejeweled.Systems;
using com.raphael.bejeweled.Systems.Event;
using com.raphael.bejeweled.Systems.Input;
using com.raphael.bejeweled.Utils;
using UnityEngine;

namespace com.raphael.bejeweled.UI
{
    public class BoardUI : MonoBehaviour
    {
        private const float _defaultPieceSize = 0.6875f;
        private const float _defaultBackgroundSize = .7f;
        
        [SerializeField]
        private Board _board = null;
        
        [SerializeField]
        private GameObject _boardBackground = null;
        
        [SerializeField]
        private AudioClip _selectPiece = null;
        [SerializeField]
        private AudioClip _flipPieces = null;
        [SerializeField]
        private AudioClip _revertFlipPieces = null;
        
        
        [SerializeField]
        private AudioClip _matchFound = null;
        private bool _playMatchSound;
        
        private PoolingSystem _poolingSystem;
        private ActionQueueSystem _actionQueueSystem;
        private AudioSystem _audioSystem;
        private InputSystem _inputSystem;
        private EventSystem _eventSystem;
        
        private Transform[] _pieces;

        private PieceFlipEvent _lastFlippedPieces;
        private PieceSelectEvent _lastSelectedPiece;

        private void Start()
        {
            _poolingSystem = SystemFactory.Resolve<PoolingSystem>();
            _actionQueueSystem = SystemFactory.Resolve<ActionQueueSystem>();
            _audioSystem = SystemFactory.Resolve<AudioSystem>();
            _inputSystem = SystemFactory.Resolve<InputSystem>();
            _eventSystem = SystemFactory.Resolve<EventSystem>();
            
            _eventSystem.Register<BoardInitializeEvent>(Initialize);
            _eventSystem.Register<PieceSelectEvent>(OnPieceSelect);

            _playMatchSound = true;
            _actionQueueSystem.Reset();
        }
        private void OnDestroy()
        {
            if (_inputSystem != null) _inputSystem.IsInteractable = true;

            if (_eventSystem == null) return;
            _eventSystem.Unregister<BoardInitializeEvent>(Initialize);
            _eventSystem.Unregister<PieceSelectEvent>(OnPieceSelect);
            _eventSystem.Unregister<PieceFlipEvent>(OnPieceChange);
            _eventSystem.Unregister<PieceVerticalReplaceEvent>(OnPieceReplace);
            _eventSystem.Unregister<PieceRemoveEvent>(OnPieceRemove);
        }

        private void Initialize(object obj)
        {
            _eventSystem.Unregister<BoardInitializeEvent>(Initialize);
            _eventSystem.Register<PieceFlipEvent>(OnPieceChange);
            _eventSystem.Register<PieceVerticalReplaceEvent>(OnPieceReplace);
            _eventSystem.Register<PieceRemoveEvent>(OnPieceRemove);
            
            _pieces = new Transform[_board.RowSize * _board.ColumnSize];
            
            for (int i = 0; i < _board.RowSize; i++)
            {
                for (int j = 0; j < _board.ColumnSize; j++)
                {
                    _pieces[(i * _board.ColumnSize) + j] =
                        CreatePiece(j, i, _board[j, i]);

                    Vector3 position = _board.GetPiecePosition(i, j);
                    Transform newPiece = Instantiate(_boardBackground, position, Quaternion.identity, transform)
                        .GetComponent<Transform>();
                    newPiece.localScale = Vector3.one * (_defaultBackgroundSize * (_board.PieceSize / _defaultPieceSize));
                }
            }
        }

        private void OnPieceChange(object obj)
        {
            var flipEvent = (PieceFlipEvent) obj;
            
            _actionQueueSystem.Enqueue(action =>
            {
                _inputSystem.IsInteractable = false;
                Transform from = GetPiece(flipEvent.FromColumn, flipEvent.FromRow);
                Vector3 fromPosition = _board.GetPiecePosition(flipEvent.FromColumn, flipEvent.FromRow);
                Transform to = GetPiece(flipEvent.ToColumn, flipEvent.ToRow);
                Vector3 toPosition = _board.GetPiecePosition(flipEvent.ToColumn, flipEvent.ToRow);

                SetPiece(from, flipEvent.ToColumn, flipEvent.ToRow);
                SetPiece(to, flipEvent.FromColumn, flipEvent.FromRow);

                from.TranslateTo(toPosition, .5f);
                to.TranslateTo(fromPosition, .5f, () =>
                {
                    _inputSystem.IsInteractable = true;
                    
                    action.Invoke();
                });

                if (_lastFlippedPieces != null && 
                    _lastFlippedPieces.FromColumn == flipEvent.FromColumn && _lastFlippedPieces.FromRow == flipEvent.FromRow &&
                    _lastFlippedPieces.ToColumn == flipEvent.ToColumn && _lastFlippedPieces.ToRow == flipEvent.ToRow)
                {
                    _lastFlippedPieces = flipEvent;
                    _audioSystem.PlayOneShot(_revertFlipPieces);
                }
                else
                {
                    _lastFlippedPieces = null;
                    _audioSystem.PlayOneShot(_flipPieces);
                }
            });
        }

        private void OnPieceReplace(object obj)
        {
            var replaceEvent = (PieceVerticalReplaceEvent) obj;
            _actionQueueSystem.Enqueue(action =>
            {
                _playMatchSound = true;
                
                _inputSystem.IsInteractable = false;
                Transform pieceTransform = replaceEvent.FromRow >= 0
                    ? GetPiece(replaceEvent.Column, replaceEvent.FromRow)
                    : null;
                
                if (pieceTransform == null)
                {
                    pieceTransform =
                        CreatePiece(replaceEvent.Column, replaceEvent.FromRow, replaceEvent.Piece);
                }
                else
                {
                    pieceTransform.GetComponent<SpriteRenderer>().sprite = _board.PiecesTypes[replaceEvent.Piece - 1];
                    SetPiece(null, replaceEvent.Column, replaceEvent.FromRow);
                }
                
                SetPiece(pieceTransform, replaceEvent.Column, replaceEvent.ToRow);
                Vector3 newPosition = _board.GetPiecePosition(replaceEvent.Column, replaceEvent.ToRow);
                pieceTransform.TranslateTo(newPosition, .5f, () =>
                {
                    _inputSystem.IsInteractable = true;
                    action.Invoke();
                });
            }, 1);
        }
        
        private void OnPieceRemove(object obj)
        {
            var removePiece = (PieceRemoveEvent) obj;
            _actionQueueSystem.Enqueue(action =>
            {
                if (_playMatchSound)
                {
                    _audioSystem.PlayOneShot(_matchFound);
                    _playMatchSound = false;
                }
                
                Transform oldPiece = GetPiece(removePiece.Column, removePiece.Row);
                
                SetPiece(null, removePiece.Column, removePiece.Row);
                
                oldPiece.ScaleTo(Vector3.zero, .2f, () =>
                {
                    _poolingSystem.Destroy(oldPiece.gameObject);

                    action.Invoke();
                });
            }, 2);
        }
        
        private void OnPieceSelect(object obj)
        {
            var pieceSelectEvent = (PieceSelectEvent) obj;
            Transform piece = GetPiece(pieceSelectEvent.Column, pieceSelectEvent.Row);
            
            _audioSystem.PlayOneShot(_selectPiece);
            
            _actionQueueSystem.Enqueue(action =>
            {
                piece.BounceScaleTo(piece.localScale, .2f, action);
            }, 3);
        }

        private Transform CreatePiece(int column, int row, int type)
        {
            var newPiece = _poolingSystem
                .Instantiate(_board.PiecesPrefab, _board.GetPiecePosition(column, row),
                    Quaternion.identity, gameObject.transform).GetComponent<Transform>();

            newPiece.localScale = Vector3.one * (.25f * (_board.PieceSize / _defaultPieceSize));
            newPiece.GetComponent<SpriteRenderer>().sprite = _board.PiecesTypes[type -1];

            return newPiece;
        }
        
        private Transform GetPiece(int column, int row)
        {
            return _pieces[GetPieceIndex(column, row)];
        }

        private int GetPieceIndex(int column, int row)
        {
            return (row * _board.ColumnSize) + column;
        }
        
        private void SetPiece(Transform piece, int column, int row)
        {
            _pieces[GetPieceIndex(column, row)] = piece;
        }
    }
}