using com.raphael.bejeweled.Systems;
using UnityEngine;
using UnityEngine.UI;

namespace com.raphael.bejeweled.UI.Button
{
    [RequireComponent(typeof(Toggle))]
    public class AudioButton : MonoBehaviour
    {
        [SerializeField]
        private AudioClip _audioButton = null;
        
        private AudioSystem _audioSystem;
        private Toggle _toggle;

        private void Start()
        {
            _audioSystem = SystemFactory.Resolve<AudioSystem>();
            _toggle = GetComponent<Toggle>();
            
            _toggle.isOn = _audioSystem.IsEnabled;
        }

        public void ToggleAudio(bool isEnabled)
        {
            _toggle.isOn = isEnabled;
            _audioSystem.SetAudioEnabled(isEnabled);
            
            _audioSystem.PlayOneShot(_audioButton);
        }
    }
}