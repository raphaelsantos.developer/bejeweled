using com.raphael.bejeweled.Gameplay;
using UnityEngine;
using UnityEngine.UI;

namespace com.raphael.bejeweled.UI.Button
{
    public class LevelButton : MonoBehaviour
    {
        [SerializeField] 
        private LevelConfig _levelConfig = null;
        
        [SerializeField]
        private GameObject _lockImage = null;

        [SerializeField]
        private GameObject _star1 = null;
        
        [SerializeField]
        private GameObject _star2 = null;
        
        [SerializeField]
        private GameObject _star3 = null;
        
        [SerializeField]
        private Text _text = null;

        private LevelConfig.Level _level;
        private int _index;

        private void Start()
        {
            _index = transform.GetSiblingIndex();
            _level = _levelConfig.Levels[_index];

            if (_index != 0 && !_level.IsUnlock)
            {
                _lockImage.SetActive(true);
                return;
            }

            _text.text = (_index + 1).ToString();

            if (_level.Stars >= 1)
            {
                _star1.SetActive(true);
                if (_level.Stars >= 2)
                {
                    _star2.SetActive(true);

                    if (_level.Stars >= 3)
                    {
                        _star3.SetActive(true);
                    }
                }
            }
        }
        
        public void LoadLevel()
        {
            if (_index != 0 && !_level.IsUnlock) return;

            _levelConfig.SetLevel(_index);
        }
    }
}