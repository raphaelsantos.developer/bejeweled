using com.raphael.bejeweled.Systems;
using UnityEngine;

namespace com.raphael.bejeweled.UI
{
    public class AudioSourceSystem : MonoBehaviour
    {
        [SerializeField] 
        private AudioClip _backgroundSound = null;
        
        private AudioSystem _audioSystem;

        private void Start()
        {
            _audioSystem = SystemFactory.Resolve<AudioSystem>();
            
            _audioSystem.Play(_backgroundSound);
        }
        
        public void PlayOneShot(AudioClip audioClip)
        {
            _audioSystem.PlayOneShot(audioClip);
        }
    }
}