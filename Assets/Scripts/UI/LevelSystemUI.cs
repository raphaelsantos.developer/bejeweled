using UnityEngine;
using UnityEngine.SceneManagement;

namespace com.raphael.bejeweled.UI
{
    public class LevelSystemUI : MonoBehaviour
    {
        public void LoadScene(int index)
        {
            SceneManager.LoadScene(index);
        }
    }
}