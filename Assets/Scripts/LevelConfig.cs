using System;
using System.Collections.Generic;
using com.raphael.bejeweled.Gameplay.Variables;
using com.raphael.bejeweled.Systems;
using com.raphael.bejeweled.Systems.Save;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace com.raphael.bejeweled.Gameplay
{
    [CreateAssetMenu(fileName = "NewLevelConfig", menuName = "Bejeweled/Level Config")]
    public class LevelConfig : ScriptableObject, ISerializationCallbackReceiver
    {
        [Serializable]
        public class Level
        {
            [SerializeField]
            private int _scoreEachPiece = 0;
            public int ScoreEachPiece => _scoreEachPiece;

            [SerializeField]
            private float _scoreToWin = 0;
            public float ScoreToWin => _scoreToWin;
        
            [SerializeField]
            private int _movesToThreeStars = 0;
        
            [SerializeField]
            private int _movesToTwoStars = 0;

            [NonSerialized]
            public float Score;
            [NonSerialized]
            public int Stars;
            [NonSerialized]
            public bool IsUnlock;

            public void UpdateLevel(float score, int move)
            {
                Score = score;
                Stars = move <= _movesToThreeStars ? 3 : move <= _movesToTwoStars ? 2 : 1;
                IsUnlock = true;
            }
        }

        [SerializeField]
        private IntegerVariable _currentLevel = null;

        [SerializeField]
        private List<Level> _levels = null;
        public List<Level> Levels => _levels;

        private SaveSystem _saveSystem;

        public Level GetCurrentLevel()
        {
            return _levels[_currentLevel.Value];
        }
        
        public void GoNextLevel()
        {
            if (_currentLevel.Value >= _levels.Count - 1)
            {
                SceneManager.LoadScene(0);
                return;
            }
            
            _currentLevel.Value++;

            SceneManager.LoadScene(1);
        }
        
        public void SetLevel(int index)
        {
            _currentLevel.Value = index;

            SceneManager.LoadScene(1);
        }

        public void LoadFromSave()
        {
            if (_saveSystem == null)
            {
                _saveSystem = SystemFactory.Resolve<SaveSystem>();
            }

            for (int i = 0; i < _levels.Count; i++)
            {
                string levelString = _saveSystem.LoadString($"level-{i}");
                if (string.IsNullOrEmpty(levelString)) break;

                Level level = _levels[i];
                string[] parameters = levelString.Split('|');

                level.IsUnlock = parameters[0] == "1";
                level.Stars = int.Parse(parameters[1]);
                level.Score = int.Parse(parameters[2]);
            }
        }
        
        public void UpdateLevel(Level level, float score, int moves)
        {
            if (_saveSystem == null)
            {
                _saveSystem = SystemFactory.Resolve<SaveSystem>();
            }

            int index = _levels.IndexOf(level);
            level.UpdateLevel(score, moves);
            _saveSystem.Save($"level-{index}", $"{Convert.ToInt32(level.IsUnlock)}|{level.Stars}|{level.Score}");

            int nextIndex = index + 1;
            Level nextLevel = nextIndex >= _levels.Count ? null : _levels[nextIndex];
            if (nextLevel != null)
            {
                nextLevel.IsUnlock = true;
                _saveSystem.Save($"level-{nextIndex}", $"{Convert.ToInt32(nextLevel.IsUnlock)}|{nextLevel.Stars}|{nextLevel.Score}");   
            }
        }

        public void OnBeforeSerialize()
        {
        }

        public void OnAfterDeserialize()
        {
            foreach (Level level in _levels)
            {
                level.IsUnlock = false;
                level.Score = 0;
                level.Stars = 0;
            }
        }
    }
}