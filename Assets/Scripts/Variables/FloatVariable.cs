using System;
using UnityEngine;

namespace com.raphael.bejeweled.Gameplay.Variables
{
    [CreateAssetMenu(fileName = "NewFloatVar", menuName = "Bejeweled/Float Variable")]
    public class FloatVariable : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField]
        private float _initialValue = 0;

        public Action<float> OnValueUpdate;

        [NonSerialized]
        private float _runtimeValue;
        public float Value
        {
            get => _runtimeValue;
            set
            {
                _runtimeValue = value;
                
                OnValueUpdate?.Invoke(value);
            }
        }

        public void OnBeforeSerialize()
        {
            
        }

        public void OnAfterDeserialize()
        {
            _runtimeValue = _initialValue;
        }
    }
}