using System;
using UnityEngine;
using UnityEngine.Events;

namespace com.raphael.bejeweled.Gameplay.Variables
{
    [CreateAssetMenu(fileName = "NewIntegerValue", menuName = "Bejeweled/Integer Variable")]
    public class IntegerVariable : ScriptableObject, ISerializationCallbackReceiver
    {
        [SerializeField]
        private int _initialValue = 0;
        
        public Action<int> OnValueUpdate;
        
        [NonSerialized]
        private int _runtimeValue;
        public int Value
        {
            get => _runtimeValue;
            set
            {
                _runtimeValue = value;
                
                OnValueUpdate?.Invoke(value);
            }
        }

        public void OnBeforeSerialize()
        {
            
        }

        public void OnAfterDeserialize()
        {
            _runtimeValue = _initialValue;
        }
    }
}